package sheridan;

import sheridan.LiquourType;

import java.util.ArrayList;
import java.util.List;

public class LiquourService {
	

    public List<String> getAvailableBrands(LiquourType type){

        List<String> brands = new ArrayList( );

        //Adrianna Vineyard, J.P. Chenet, Barefoot, Spumante Bambino, Yellowglen
        if(type.equals(LiquourType.WINE)){
            brands.add("Adrianna Vineyard");
            brands.add(("J. P. Chenet"));
            brands.add("Barefoot");
            brands.add("Spumante Bambino");
            brands.add("Yellowglen");

          //Glenfiddich, Johnnie Walker, Jack Daniel's, Seagrams, Buchanan's
        }else if(type.equals(LiquourType.WHISKY)){
            brands.add("Glenfiddich");
            brands.add("Johnnie Walker");
            brands.add("Jack Daniel's");
            brands.add("Seagrams");
            brands.add("Buchanan's");

            //Corona, Budweiser, Heineken, Coors, Canadian
        }else if(type.equals(LiquourType.BEER)){
            brands.add("Corona");
            brands.add("Budweiser");
            brands.add("Heineken");
            brands.add("Coors");
            brands.add("Canadian");

        }else {
            brands.add("No Brand Available");
        }
    return brands;
    }
}
